#!/bin/bash

apt -y install qemu-kvm bridge-utils libguestfs-tools

mkdir -p /vm/os
mkdir -p /vm/images
mkdir -p /etc/qemu
if [ ! -f /vm/os/debian-10-generic-amd64-20210721-710.raw ]; then
  wget http://cloud.debian.org/images/cloud/buster/20210721-710/debian-10-generic-amd64-20210721-710.qcow2 -P /vm/os
  qemu-img convert -f qcow2 -O raw /vm/os/debian-10-generic-amd64-20210721-710.qcow2 /vm/os/debian-10-generic-amd64-20210721-710.raw
  rm /vm/os/debian-10-generic-amd64-20210721-710.qcow2
fi
cat > /etc/qemu/bridge.conf << EOF
allow br0
EOF
