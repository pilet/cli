#!/bin/bash

NAME=$1
MAC=$2
IP=$3
HDD=${4:-80}
RAM=${5:-4}
CONFIG=${6:-default}
VMPATH="/vm/images/$NAME"

# ---- HOST CONFIG

GATEWAY_IP="54.39.18.254"
CONFIG="/root/cli/cloud-config/${CONFIG}.sh"
SSH_KEY=$(cat /root/.ssh/authorized_keys)
CONFIG_64=$(base64 -w0 < $CONFIG)


if [[ $# -lt 3 ]] ; then
    echo 'Usage : vm-build name mac ip [disk ram config]'
    exit 0
fi

mkdir -p $VMPATH

cp /vm/os/debian-10-generic-amd64-20210721-710.raw $VMPATH/disk.0
qemu-img resize $VMPATH/disk.0 +${HDD}G

cat > $VMPATH/user-data <<EOF
#cloud-config
password: parol2021
chpasswd: { expire: False }
ssh_pwauth: True
disable_root: False
runcmd:
  - mkdir -p /root/.ssh
  - mkdir -p /root/.init
  - echo "${SSH_KEY}" > /root/.ssh/authorized_keys
  - service sshd restart
  - apt update
  - hostnamectl set-hostname ${NAME}.pilet.io
  - echo $CONFIG_64 | base64 --decode > /root/.init/setup.sh
  - chmod +x /root/.init/setup.sh
  - /root/.init/setup.sh > /root/.init/setup.log
EOF


cat > $VMPATH/network-config <<EOF
version: 1
config:
  - type: physical
    name: ens3
    mac_address: $MAC
    subnets:
       - type: static
         address: $IP/32
         gateway: $GATEWAY_IP
  - type: nameserver
    address:
      - 213.186.33.99
      - 8.8.8.8
EOF


cat > $VMPATH/start <<EOF
qemu-system-x86_64\
 -name $NAME --enable-kvm -cpu host\
 -smp cpus=2 -m ${RAM}G\
 -vnc :2\
 -drive file=$VMPATH/disk.0,if=virtio,index=0,format=raw\
 -drive file=$VMPATH/init.img,if=virtio,index=1,format=raw\
 -net nic,model=virtio,macaddr=${MAC}\
 -net bridge,br=br0 --daemonize
EOF

chmod +x $VMPATH/start

cloud-localds -v --network-config=$VMPATH/network-config $VMPATH/init.img $VMPATH/user-data
